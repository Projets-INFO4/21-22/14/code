use dioxus::prelude::*;
use wasm_bindgen::__rt::core::ptr::null;
use wasm_bindgen::__rt::std::collections::HashMap;
use wasm_bindgen::__rt::std::process::exit;


pub(crate) fn app(cx: Scope) -> Element {

    let (home, _set_home) = use_state(&cx, || "home");
    let (implementation, _set_implementation) = use_state(&cx, || "implementations");
    let (first_active, _set_first_active) = use_state(&cx, || "");
    let (second_active, _set_second_active) = use_state(&cx, || "active");
    let (third_active, _set_third_active) = use_state(&cx, || "");
    let (feed_active, _set_feed_active) = use_state(&cx, || "second");
    let (email, _set_email) = use_state(&cx, || "---".to_string());
    let (username, _set_username) = use_state(&cx, || "---".to_string());
    let (password, _set_password) = use_state(&cx, || "---".to_string());
    //let ( first, _set_first ) = use_state(&cx, || "first");
    let mut component = rsx! {
        div {
            class : "mele",
            onclick : move |evt| _set_home("login"),
            "{home}"
        }
    };
    let home_user = rsx! {
        div{
            class : "main home",
            div {
                class : "header",
                a{
                    "href" : "#",
                    class : "title",
                    onclick : move |evt| _set_home("home"),
                    "Conduit"
                }
                div {
                    class : "nav",
                    a{
                        "href" : "#",
                        class : "item active",
                        onclick : move |evt| _set_home("home"),
                        "Home"
                    }
                    a{
                        "href" : "#",
                        class : "item",
                        onclick : move |evt| _set_home("login"),
                        "Sign in"
                    }
                    a{
                        "href" : "#",
                        class : "item",
                        onclick : move |evt| _set_home("register"),
                        "Sign up"
                    }
                }
            },
            div {
                class : "header_description",
                div {
                    class : "title",
                    "conduit"
                }
                div{
                    class : "description",
                    "A place to share your knowledge"
                }
            }
            div {
                class : "body",
                div {
                    class : "feeds",
                    div{
                        class : "nav",
                        div {
                            class : "item first {first_active}",
                            onclick : move |evt| { _set_first_active("active"); _set_second_active(""); _set_third_active(""); _set_feed_active("first");},
                            "YOUR FEEDS"
                        }
                        div {
                            class : "item second {second_active}",
                            onclick : move |evt| { _set_first_active(""); _set_second_active("active"); _set_third_active(""); _set_feed_active("second");},
                            "GLOBAL FEEDS"
                        }
                        div {
                            class : "item third {third_active}",
                            onclick : move |evt| { _set_third_active("active"); _set_second_active(""); _set_first_active(""); _set_feed_active("third"); },
                            "ANOTHER SEARCH"
                        }
                    }
                    div {
                        class : "content {feed_active}",
                        div {
                            class : "item_ first",
                            "ALL YOUR FEEDS"
                        }
                        div {
                            class : "item_ second",
                            div{
                                class : "gf tgf",
                                div{
                                    class : "container",
                                    div {
                                        class : "headerH",
                                        div {
                                            class : "leftH",
                                            a {
                                                href : "#",
                                                class : "logo",
                                                img {
                                                    src : "img/profil.png",
                                                }
                                            }
                                            div {
                                                class : "user",
                                                a {
                                                    href : "#",
                                                    class : "name",
                                                    "Genome",
                                                }
                                                div {
                                                    class : "date",
                                                    "November 24, 2021"
                                                }
                                            }
                                        }
                                        a {
                                            href : "#",
                                            class : "right",
                                            onclick : move |evt| { _set_home("implementations");},
                                            div {
                                                class : "logo",
                                                "L",
                                            }
                                            div {
                                                class : "likes",
                                                "1503",
                                            }
                                        }
                                    }
                                    a {
                                        class : "bodyH",
                                        href : "#",
                                        onclick : move |evt| { _set_home("implementations");},
                                        div {
                                            class : "title",
                                            "Create A New Implementation",
                                        }
                                        div {
                                            class : "subtitle",
                                            "join the community br creating a new implementation",
                                        }
                                    }
                                    div{
                                        class : "footerH",
                                        a {
                                            class : "left",
                                            href : "#",
                                            onclick : move |evt| { _set_home("implementations");},
                                            "Read More ..."
                                        }
                                        div {
                                            class : "right",
                                            a {
                                                href : "#",
                                                class : "itemf",
                                                onclick : move |evt| { _set_home("implementations");},
                                                "implementations"
                                            }
                                        }
                                    }
                                }
                            }
                            div{
                                class : "gf tgf",
                                div{
                                    class : "container",
                                    div {
                                        class : "headerH",
                                        div {
                                            class : "leftH",
                                            a {
                                                href : "#",
                                                class : "logo",
                                                img {
                                                    src : "img/profil.png",
                                                }
                                            }
                                            div {
                                                class : "user",
                                                a {
                                                    href : "#",
                                                    class : "name",
                                                    "Genome",
                                                }
                                                div {
                                                    class : "date",
                                                    "November 24, 2021"
                                                }
                                            }
                                        }
                                        a {
                                            href : "#",
                                            class : "right",
                                            div {
                                                class : "logo",
                                                "L",
                                            }
                                            div {
                                                class : "likes",
                                                "676",
                                            }
                                        }
                                    }
                                    a {
                                        class : "bodyH",
                                        href : "#",
                                        onclick : move |evt| { _set_home("exit_implementation");},
                                        div {
                                            class : "title",
                                            "Explore Implementations",
                                        }
                                        div {
                                            class : "subtitle",
                                            "discover the implementations created by the RealWorld community",
                                        }
                                    }
                                    div{
                                        class : "footerH",
                                        a {
                                            class : "left",
                                            href : "#",
                                            onclick : move |evt| { _set_home("exit_implementation");},
                                            "Read More ..."
                                        }
                                        div {
                                            class : "right",
                                            a {
                                                href : "#",
                                                class : "itemf",
                                                onclick : move |evt| { _set_home("exit_implementation");},
                                                "codebaseShow"
                                            }
                                            a {
                                                href : "#",
                                                class : "itemf",
                                                onclick : move |evt| { _set_home("exit_implementation");},
                                                "implementations"
                                            }
                                        }
                                    }
                                }
                            }
                            div{
                                class : "gf tgf",
                                div{
                                    class : "container",
                                    div {
                                        class : "headerH",
                                        div {
                                            class : "leftH",
                                            a {
                                                href : "#",
                                                class : "logo",
                                                img {
                                                    src : "img/profil.png",
                                                }
                                            }
                                            div {
                                                class : "user",
                                                a {
                                                    href : "#",
                                                    class : "name",
                                                    "Genome",
                                                }
                                                div {
                                                    class : "date",
                                                    "November 24, 2021"
                                                }
                                            }
                                        }
                                        a {
                                            href : "#",
                                            class : "right",
                                            div {
                                                class : "logo",
                                                "L",
                                            }
                                            div {
                                                class : "likes",
                                                "676",
                                            }
                                        }
                                    }
                                    a {
                                        class : "bodyH",
                                        href : "#",
                                        onclick : move |evt| { _set_home("explore_realworld");},
                                        div {
                                            class : "title",
                                            "Welcome to RealWorld Project",
                                        }
                                        div {
                                            class : "subtitle",
                                            "Exemplary fullstack Medium.com clone powered by React, Angular, Node, Django, and many more",
                                        }
                                    }
                                    div{
                                        class : "footerH",
                                        a {
                                            class : "left",
                                            href : "#",
                                            onclick : move |evt| { _set_home("explore_realworld");},
                                            "Read More ..."
                                        }
                                        div {
                                            class : "right",
                                            a {
                                                href : "#",
                                                class : "itemf",
                                                onclick : move |evt| { _set_home("explore_realworld");},
                                                "welcome"
                                            }
                                            a {
                                                href : "#",
                                                class : "itemf",
                                                onclick : move |evt| { _set_home("explore_realworld");},
                                                "introduction"
                                            }
                                        }
                                    }
                                }
                            }
                            div{
                                class : "gf tgf",
                            }
                        }
                        div {
                            class : "item_ third",
                            "ALL ANOTHER SEARCH"
                        }
                    }
                }
                div {
                    class : "aside",
                    div{
                        class : "title",
                        "Popular Tags"
                    }
                    a{
                        class : "title",
                        "href" : "#",
                        class : "tag",
                        "welcome"
                    }
                    a{
                        class : "title",
                        "href" : "#",
                        class : "tag",
                        "implementations"
                    }
                    a{
                        class : "title",
                        "href" : "#",
                        class : "tag",
                        "codebaseShow"
                    }
                    a{
                        class : "title",
                        "href" : "#",
                        class : "tag",
                        "introduction"
                    }
                }
            }
            a {
                class : "footer",
                "href" : "https://github.com/gothinkster/angularjs-realworld-example-app",
                target : "_blank",
                span {
                    class : "logo",
                    svg{
                        xmlns : "http://www.w3.org/2000/svg",
                        "viewBox" : "0 0 496 512",
                        fill : "white",
                        path {
                            d : "M165.9 397.4c0 2-2.3 3.6-5.2 3.6-3.3.3-5.6-1.3-5.6-3.6 0-2 2.3-3.6 5.2-3.6 3-.3 5.6 1.3 5.6 3.6zm-31.1-4.5c-.7 2 1.3 4.3 4.3 4.9 2.6 1 5.6 0 6.2-2s-1.3-4.3-4.3-5.2c-2.6-.7-5.5.3-6.2 2.3zm44.2-1.7c-2.9.7-4.9 2.6-4.6 4.9.3 2 2.9 3.3 5.9 2.6 2.9-.7 4.9-2.6 4.6-4.6-.3-1.9-3-3.2-5.9-2.9zM244.8 8C106.1 8 0 113.3 0 252c0 110.9 69.8 205.8 169.5 239.2 12.8 2.3 17.3-5.6 17.3-12.1 0-6.2-.3-40.4-.3-61.4 0 0-70 15-84.7-29.8 0 0-11.4-29.1-27.8-36.6 0 0-22.9-15.7 1.6-15.4 0 0 24.9 2 38.6 25.8 21.9 38.6 58.6 27.5 72.9 20.9 2.3-16 8.8-27.1 16-33.7-55.9-6.2-112.3-14.3-112.3-110.5 0-27.5 7.6-41.3 23.6-58.9-2.6-6.5-11.1-33.3 2.6-67.9 20.9-6.5 69 27 69 27 20-5.6 41.5-8.5 62.8-8.5s42.8 2.9 62.8 8.5c0 0 48.1-33.6 69-27 13.7 34.7 5.2 61.4 2.6 67.9 16 17.7 25.8 31.5 25.8 58.9 0 96.5-58.9 104.2-114.8 110.5 9.2 7.9 17 22.9 17 46.4 0 33.7-.3 75.4-.3 83.6 0 6.5 4.6 14.4 17.3 12.1C428.2 457.8 496 362.9 496 252 496 113.3 383.5 8 244.8 8zM97.2 352.9c-1.3 1-1 3.3.7 5.2 1.6 1.6 3.9 2.3 5.2 1 1.3-1 1-3.3-.7-5.2-1.6-1.6-3.9-2.3-5.2-1zm-10.8-8.1c-.7 1.3.3 2.9 2.3 3.9 1.6 1 3.6.7 4.3-.7.7-1.3-.3-2.9-2.3-3.9-2-.6-3.6-.3-4.3.7zm32.4 35.6c-1.6 1.3-1 4.3 1.3 6.2 2.3 2.3 5.2 2.6 6.5 1 1.3-1.3.7-4.3-1.3-6.2-2.2-2.3-5.2-2.6-6.5-1zm-11.4-14.7c-1.6 1-1.6 3.6 0 5.9 1.6 2.3 4.3 3.3 5.6 2.3 1.6-1.3 1.6-3.9 0-6.2-1.4-2.3-4-3.3-5.6-2z"
                        }
                    }
                }
                span{
                    class : "title",
                    "Fork On Github"
                }

            }

        }
    };
    let new_implementation = rsx! {
        div{
            class : "implementation home",
            div {
                class : "header",
                a{
                    "href" : "#",
                    class : "title",
                    onclick : move |evt| _set_home("home"),
                    "Conduit"
                }
                div {
                    class : "nav",
                    a{
                        "href" : "#",
                        class : "item active",
                        onclick : move |evt| _set_home("home"),
                        "Home"
                    }
                    a{
                        "href" : "#",
                        class : "item",
                        onclick : move |evt| _set_home("login"),
                        "Sign in"
                    }
                    a{
                        "href" : "#",
                        class : "item",
                        onclick : move |evt| _set_home("register"),
                        "Sign up"
                    }
                }
            },
            div {
                class : "header_description",
                div {
                    class : "title",
                    "Create A New implementation"
                }
                div{
                    class : "description",
                    div{
                        class : "user",
                        a{
                            href : "#",
                            class : "logo",
                            img{
                                src : "img/profil.png",
                            }
                        }
                        div{
                            class : "infos",
                            a {
                                href : "#",
                                class : "name",
                                "Genome"
                            }
                            div{
                                class : "date",
                                "November 24, 2021"
                            }
                        }
                    }
                    a {
                        class : "follow",
                        href : "#",
                        div{
                            class : "logo",
                            "+"
                        }
                        div {
                            class : "foll",
                            "Follow Gerome"
                        }
                    }
                    a {
                        class : "article",
                        href : "#",
                        div{
                            class : "logo",
                            "+"
                        }
                        div {
                            class : "fav",
                            "Favorite Article (1588)"
                        }
                    }
                }
            }
            div {
                class : "body_article",
                div {
                    class : "top",
                    div {
                        class : "description",
                        "Share your knowledge and enpower the community by creating a new implementation"
                    }
                    div {
                        class : "tags",
                        div {
                            class : "tag",
                            "implementations",
                        }
                    }
                }
                div {
                    class : "main",
                    div {
                        class : "descriptions",
                        div {
                            class : "user",
                            div {
                                class : "logo",
                                img{
                                    src : "img/profil.png",
                                }
                            }
                            div {
                                class : "infos",
                                div {
                                    class : "name",
                                    "Gerome"
                                }
                                div {
                                    class : "date",
                                    "November 24, 2021"
                                }
                            }
                        }

                        div {
                            class : "follow",
                            div {
                                 class : "logo",
                                 "+"
                            }
                            div {
                                 class : "foll",
                                 "Follow Gerome"
                            }
                        }

                        div {
                            class : "article",
                            div {
                                 class : "logo",
                                 "+"
                            }
                            div {
                                 class : "fav",
                                 "Favorite Article (1588)"
                            }
                        }
                    }
                    div {
                        class : "articles",
                        div{
                            class : "description",
                            "Sign in or Sign up to add comment on this article"
                        }
                        div {
                            class : "article",
                            div{
                                class : "description",
                                "If someone else has started working on an implementation, consider jumping in and helping them! by contacting the author."
                            }
                            div {
                                class : "user",
                                div {
                                    class : "logo",
                                    img {
                                        src : "img/profil.png",
                                    }
                                }
                                div {
                                    class : "name",
                                    "Gerome"
                                }
                                div{
                                    class : "date",
                                    "November 24, 2021",
                                }
                            }
                        }
                        div {
                            class : "article",
                            div{
                                class : "description",
                                "If someone else has started working on an implementation, consider jumping in and helping them! by contacting the author."
                            }
                            div {
                                class : "user",
                                div {
                                    class : "logo",
                                    img {
                                        src : "img/profil.png",
                                    }
                                }
                                div {
                                    class : "name",
                                    "Gerome"
                                }
                                div{
                                    class : "date",
                                    "November 24, 2021",
                                }
                            }
                        }

                    }
                }
            }
            a {
                class : "footer",
                "href" : "https://github.com/gothinkster/angularjs-realworld-example-app",
                target : "_blank",
                span {
                    class : "logo",
                    svg{
                        xmlns : "http://www.w3.org/2000/svg",
                        "viewBox" : "0 0 496 512",
                        fill : "white",
                        path {
                            d : "M165.9 397.4c0 2-2.3 3.6-5.2 3.6-3.3.3-5.6-1.3-5.6-3.6 0-2 2.3-3.6 5.2-3.6 3-.3 5.6 1.3 5.6 3.6zm-31.1-4.5c-.7 2 1.3 4.3 4.3 4.9 2.6 1 5.6 0 6.2-2s-1.3-4.3-4.3-5.2c-2.6-.7-5.5.3-6.2 2.3zm44.2-1.7c-2.9.7-4.9 2.6-4.6 4.9.3 2 2.9 3.3 5.9 2.6 2.9-.7 4.9-2.6 4.6-4.6-.3-1.9-3-3.2-5.9-2.9zM244.8 8C106.1 8 0 113.3 0 252c0 110.9 69.8 205.8 169.5 239.2 12.8 2.3 17.3-5.6 17.3-12.1 0-6.2-.3-40.4-.3-61.4 0 0-70 15-84.7-29.8 0 0-11.4-29.1-27.8-36.6 0 0-22.9-15.7 1.6-15.4 0 0 24.9 2 38.6 25.8 21.9 38.6 58.6 27.5 72.9 20.9 2.3-16 8.8-27.1 16-33.7-55.9-6.2-112.3-14.3-112.3-110.5 0-27.5 7.6-41.3 23.6-58.9-2.6-6.5-11.1-33.3 2.6-67.9 20.9-6.5 69 27 69 27 20-5.6 41.5-8.5 62.8-8.5s42.8 2.9 62.8 8.5c0 0 48.1-33.6 69-27 13.7 34.7 5.2 61.4 2.6 67.9 16 17.7 25.8 31.5 25.8 58.9 0 96.5-58.9 104.2-114.8 110.5 9.2 7.9 17 22.9 17 46.4 0 33.7-.3 75.4-.3 83.6 0 6.5 4.6 14.4 17.3 12.1C428.2 457.8 496 362.9 496 252 496 113.3 383.5 8 244.8 8zM97.2 352.9c-1.3 1-1 3.3.7 5.2 1.6 1.6 3.9 2.3 5.2 1 1.3-1 1-3.3-.7-5.2-1.6-1.6-3.9-2.3-5.2-1zm-10.8-8.1c-.7 1.3.3 2.9 2.3 3.9 1.6 1 3.6.7 4.3-.7.7-1.3-.3-2.9-2.3-3.9-2-.6-3.6-.3-4.3.7zm32.4 35.6c-1.6 1.3-1 4.3 1.3 6.2 2.3 2.3 5.2 2.6 6.5 1 1.3-1.3.7-4.3-1.3-6.2-2.2-2.3-5.2-2.6-6.5-1zm-11.4-14.7c-1.6 1-1.6 3.6 0 5.9 1.6 2.3 4.3 3.3 5.6 2.3 1.6-1.3 1.6-3.9 0-6.2-1.4-2.3-4-3.3-5.6-2z"
                        }
                    }
                }
                span{
                    class : "title",
                    "Fork On Github"
                }

            }

        }
    };
    let login_user = rsx! {
        div{
            class : "main",
            div {
                class : "header",
                a{
                    "href" : "#",
                    class : "title",
                    onclick : move |evt| _set_home("home"),
                    "Conduit"
                }
                div {
                    class : "nav",
                    a{
                        "href" : "#",
                        class : "item",
                        onclick : move |evt| _set_home("home"),
                        "Home"
                    }
                    a{
                        "href" : "#",
                        class : "item active",
                        onclick : move |evt| _set_home("login"),
                        "Sign in"
                    }
                    a{
                        "href" : "#",
                        class : "item",
                        onclick : move |evt| _set_home("register"),
                        "Sign up"
                    }
                }
            },
            div {
                class : "body",
                h2{
                    class : "item title",
                    "Sign in"
                }
                a{
                    class : "item register",
                    href : "#",
                    onclick : move |evt| _set_home("register"),
                    "Need An Account?"
                }
                form {
                    class : "item form",
                    name : "form",

                    input{
                        "type" : "email",
                        class : "input email",
                        name : "email",
                        required : "required",
                        value : "{email}",
                        onchange : move |evt| _set_email(evt.value.clone()),
                        placeholder : "Email"
                    }
                    input{
                        "type" : "password",
                        class : "input password",
                        name : "password",
                        required : "required",
                        value : "{password}",
                        onchange : move |evt| _set_password(evt.value.clone()),
                        placeholder : "Password"
                    }
                    button{
                        class : "input submit",
                        onclick : move |evt| _set_home("home"),
                        "Sign in"
                    }
                }
            }
            a {
                class : "footer",
                "href" : "https://github.com/gothinkster/angularjs-realworld-example-app",
                target : "_blank",
                span {
                    class : "logo",
                    svg{
                        xmlns : "http://www.w3.org/2000/svg",
                        "viewBox" : "0 0 496 512",
                        fill : "white",
                        path {
                            d : "M165.9 397.4c0 2-2.3 3.6-5.2 3.6-3.3.3-5.6-1.3-5.6-3.6 0-2 2.3-3.6 5.2-3.6 3-.3 5.6 1.3 5.6 3.6zm-31.1-4.5c-.7 2 1.3 4.3 4.3 4.9 2.6 1 5.6 0 6.2-2s-1.3-4.3-4.3-5.2c-2.6-.7-5.5.3-6.2 2.3zm44.2-1.7c-2.9.7-4.9 2.6-4.6 4.9.3 2 2.9 3.3 5.9 2.6 2.9-.7 4.9-2.6 4.6-4.6-.3-1.9-3-3.2-5.9-2.9zM244.8 8C106.1 8 0 113.3 0 252c0 110.9 69.8 205.8 169.5 239.2 12.8 2.3 17.3-5.6 17.3-12.1 0-6.2-.3-40.4-.3-61.4 0 0-70 15-84.7-29.8 0 0-11.4-29.1-27.8-36.6 0 0-22.9-15.7 1.6-15.4 0 0 24.9 2 38.6 25.8 21.9 38.6 58.6 27.5 72.9 20.9 2.3-16 8.8-27.1 16-33.7-55.9-6.2-112.3-14.3-112.3-110.5 0-27.5 7.6-41.3 23.6-58.9-2.6-6.5-11.1-33.3 2.6-67.9 20.9-6.5 69 27 69 27 20-5.6 41.5-8.5 62.8-8.5s42.8 2.9 62.8 8.5c0 0 48.1-33.6 69-27 13.7 34.7 5.2 61.4 2.6 67.9 16 17.7 25.8 31.5 25.8 58.9 0 96.5-58.9 104.2-114.8 110.5 9.2 7.9 17 22.9 17 46.4 0 33.7-.3 75.4-.3 83.6 0 6.5 4.6 14.4 17.3 12.1C428.2 457.8 496 362.9 496 252 496 113.3 383.5 8 244.8 8zM97.2 352.9c-1.3 1-1 3.3.7 5.2 1.6 1.6 3.9 2.3 5.2 1 1.3-1 1-3.3-.7-5.2-1.6-1.6-3.9-2.3-5.2-1zm-10.8-8.1c-.7 1.3.3 2.9 2.3 3.9 1.6 1 3.6.7 4.3-.7.7-1.3-.3-2.9-2.3-3.9-2-.6-3.6-.3-4.3.7zm32.4 35.6c-1.6 1.3-1 4.3 1.3 6.2 2.3 2.3 5.2 2.6 6.5 1 1.3-1.3.7-4.3-1.3-6.2-2.2-2.3-5.2-2.6-6.5-1zm-11.4-14.7c-1.6 1-1.6 3.6 0 5.9 1.6 2.3 4.3 3.3 5.6 2.3 1.6-1.3 1.6-3.9 0-6.2-1.4-2.3-4-3.3-5.6-2z"
                        }
                    }
                }
                span{
                    class : "title",
                    "Fork On Github"
                }

            }

        }
    };
    let register_user = rsx! {
        div{
            class : "main",
            div {
                class : "header",
                a{
                    "href" : "#",
                    class : "title",
                    onclick : move |evt| _set_home("home"),
                    "Conduit"
                }
                div {
                    class : "nav",
                    a{
                        "href" : "#",
                        class : "item",
                        onclick : move |evt| _set_home("home"),
                        "Home"
                    }
                    a{
                        "href" : "#",
                        class : "item",
                        onclick : move |evt| _set_home("login"),
                        "Sign in"
                    }
                    a{
                        "href" : "#",
                        class : "item active",
                        onclick : move |evt| _set_home("register"),
                        "Sign up"
                    }
                }
            },
            div {
                class : "body",
                h2{
                    class : "item title",
                    "Sign up"
                }
                a{
                    class : "item login",
                    href : "#",
                    onclick : move |evt| _set_home("login"),
                    "Have an account?"
                }
                form {
                    class : "item form",
                    name : "form",
                    input{
                        "type" : "text",
                        class : "input username",
                        name : "username",
                        placeholder : "Username",
                        required : "required",
                        value : "{username}",
                        onchange : move |evt| _set_username(evt.value.clone()),
                    }
                    input{
                        "type" : "email",
                        class : "input email",
                        name : "email",
                        required : "required",
                        value : "{email}",
                        onchange : move |evt| _set_email(evt.value.clone()),
                        placeholder : "Email"
                    }
                    input{
                        "type" : "password",
                        class : "input password",
                        name : "password",
                        required : "required",
                        value : "{password}",
                        onchange : move |evt| _set_password(evt.value.clone()),
                        placeholder : "Password"
                    }
                    button{
                        class : "input submit",
                        onclick : move |evt| _set_home("login"),
                        "Sign up"
                    }
                }
            }
            a {
                class : "footer",
                "href" : "https://github.com/gothinkster/angularjs-realworld-example-app",
                target : "_blank",
                span {
                    class : "logo",
                    svg{
                        xmlns : "http://www.w3.org/2000/svg",
                        "viewBox" : "0 0 496 512",
                        fill : "white",
                        path {
                            d : "M165.9 397.4c0 2-2.3 3.6-5.2 3.6-3.3.3-5.6-1.3-5.6-3.6 0-2 2.3-3.6 5.2-3.6 3-.3 5.6 1.3 5.6 3.6zm-31.1-4.5c-.7 2 1.3 4.3 4.3 4.9 2.6 1 5.6 0 6.2-2s-1.3-4.3-4.3-5.2c-2.6-.7-5.5.3-6.2 2.3zm44.2-1.7c-2.9.7-4.9 2.6-4.6 4.9.3 2 2.9 3.3 5.9 2.6 2.9-.7 4.9-2.6 4.6-4.6-.3-1.9-3-3.2-5.9-2.9zM244.8 8C106.1 8 0 113.3 0 252c0 110.9 69.8 205.8 169.5 239.2 12.8 2.3 17.3-5.6 17.3-12.1 0-6.2-.3-40.4-.3-61.4 0 0-70 15-84.7-29.8 0 0-11.4-29.1-27.8-36.6 0 0-22.9-15.7 1.6-15.4 0 0 24.9 2 38.6 25.8 21.9 38.6 58.6 27.5 72.9 20.9 2.3-16 8.8-27.1 16-33.7-55.9-6.2-112.3-14.3-112.3-110.5 0-27.5 7.6-41.3 23.6-58.9-2.6-6.5-11.1-33.3 2.6-67.9 20.9-6.5 69 27 69 27 20-5.6 41.5-8.5 62.8-8.5s42.8 2.9 62.8 8.5c0 0 48.1-33.6 69-27 13.7 34.7 5.2 61.4 2.6 67.9 16 17.7 25.8 31.5 25.8 58.9 0 96.5-58.9 104.2-114.8 110.5 9.2 7.9 17 22.9 17 46.4 0 33.7-.3 75.4-.3 83.6 0 6.5 4.6 14.4 17.3 12.1C428.2 457.8 496 362.9 496 252 496 113.3 383.5 8 244.8 8zM97.2 352.9c-1.3 1-1 3.3.7 5.2 1.6 1.6 3.9 2.3 5.2 1 1.3-1 1-3.3-.7-5.2-1.6-1.6-3.9-2.3-5.2-1zm-10.8-8.1c-.7 1.3.3 2.9 2.3 3.9 1.6 1 3.6.7 4.3-.7.7-1.3-.3-2.9-2.3-3.9-2-.6-3.6-.3-4.3.7zm32.4 35.6c-1.6 1.3-1 4.3 1.3 6.2 2.3 2.3 5.2 2.6 6.5 1 1.3-1.3.7-4.3-1.3-6.2-2.2-2.3-5.2-2.6-6.5-1zm-11.4-14.7c-1.6 1-1.6 3.6 0 5.9 1.6 2.3 4.3 3.3 5.6 2.3 1.6-1.3 1.6-3.9 0-6.2-1.4-2.3-4-3.3-5.6-2z"
                        }
                    }
                }
                span{
                    class : "title",
                    "Fork On Github"
                }

            }
        }
    };
    let exit_implementation = rsx! {
        div{
            class : "implementation home",
            div {
                class : "header",
                a{
                    "href" : "#",
                    class : "title",
                    onclick : move |evt| _set_home("home"),
                    "Conduit"
                }
                div {
                    class : "nav",
                    a{
                        "href" : "#",
                        class : "item active",
                        onclick : move |evt| _set_home("home"),
                        "Home"
                    }
                    a{
                        "href" : "#",
                        class : "item",
                        onclick : move |evt| _set_home("login"),
                        "Sign in"
                    }
                    a{
                        "href" : "#",
                        class : "item",
                        onclick : move |evt| _set_home("register"),
                        "Sign up"
                    }
                }
            },
            div {
                class : "header_description",
                div {
                    class : "title",
                    "Explore implementations"
                }
                div{
                    class : "description",
                    div{
                        class : "user",
                        a{
                            href : "#",
                            class : "logo",
                            img{
                                src : "img/profil.png",
                            }
                        }
                        div{
                            class : "infos",
                            a {
                                href : "#",
                                class : "name",
                                "Genome"
                            }
                            div{
                                class : "date",
                                "November 24, 2021"
                            }
                        }
                    }
                    a {
                        class : "follow",
                        href : "#",
                        div{
                            class : "logo",
                            "+"
                        }
                        div {
                            class : "foll",
                            "Follow Gerome"
                        }
                    }
                    a {
                        class : "article",
                        href : "#",
                        div{
                            class : "logo",
                            "+"
                        }
                        div {
                            class : "fav",
                            "Favorite Article (1588)"
                        }
                    }
                }
            }
            div {
                class : "body_article",
                div {
                    class : "top",
                    div {
                        class : "description",
                        div {
                            "Over 100 implementations have been created using various languages, libraries, and frameworks."
                        }
                        div {
                            "Explore them on CodebaseShow."
                        }
                    }
                    div {
                        class : "tags",
                        div {
                            class : "tag",
                            "codebaseShow",
                        }
                        div {
                            class : "tag",
                            "implementations",
                        }
                    }
                }
                div {
                    class : "main",
                    div {
                        class : "descriptions",
                        div {
                            class : "user",
                            div {
                                class : "logo",
                                img{
                                    src : "img/profil.png",
                                }
                            }
                            div {
                                class : "infos",
                                div {
                                    class : "name",
                                    "Gerome"
                                }
                                div {
                                    class : "date",
                                    "November 24, 2021"
                                }
                            }
                        }

                        div {
                            class : "follow",
                            div {
                                 class : "logo",
                                 "+"
                            }
                            div {
                                 class : "foll",
                                 "Follow Gerome"
                            }
                        }

                        div {
                            class : "article",
                            div {
                                 class : "logo",
                                 "+"
                            }
                            div {
                                 class : "fav",
                                 "Favorite Article (1588)"
                            }
                        }
                    }
                    div {
                        class : "articles",
                        div{
                            class : "description",
                            "Sign in or Sign up to add comment on this article"
                        }
                        div {
                            class : "article",
                            div{
                                class : "description",
                                "There are 3 categories: Frontend, Backend and FullStack."
                            }
                            div {
                                class : "user",
                                div {
                                    class : "logo",
                                    img {
                                        src : "img/profil.png",
                                    }
                                }
                                div {
                                    class : "name",
                                    "Gerome"
                                }
                                div{
                                    class : "date",
                                    "November 24, 2021",
                                }
                            }
                        }


                    }
                }
            }
            a {
                class : "footer",
                "href" : "https://github.com/gothinkster/angularjs-realworld-example-app",
                target : "_blank",
                span {
                    class : "logo",
                    svg{
                        xmlns : "http://www.w3.org/2000/svg",
                        "viewBox" : "0 0 496 512",
                        fill : "white",
                        path {
                            d : "M165.9 397.4c0 2-2.3 3.6-5.2 3.6-3.3.3-5.6-1.3-5.6-3.6 0-2 2.3-3.6 5.2-3.6 3-.3 5.6 1.3 5.6 3.6zm-31.1-4.5c-.7 2 1.3 4.3 4.3 4.9 2.6 1 5.6 0 6.2-2s-1.3-4.3-4.3-5.2c-2.6-.7-5.5.3-6.2 2.3zm44.2-1.7c-2.9.7-4.9 2.6-4.6 4.9.3 2 2.9 3.3 5.9 2.6 2.9-.7 4.9-2.6 4.6-4.6-.3-1.9-3-3.2-5.9-2.9zM244.8 8C106.1 8 0 113.3 0 252c0 110.9 69.8 205.8 169.5 239.2 12.8 2.3 17.3-5.6 17.3-12.1 0-6.2-.3-40.4-.3-61.4 0 0-70 15-84.7-29.8 0 0-11.4-29.1-27.8-36.6 0 0-22.9-15.7 1.6-15.4 0 0 24.9 2 38.6 25.8 21.9 38.6 58.6 27.5 72.9 20.9 2.3-16 8.8-27.1 16-33.7-55.9-6.2-112.3-14.3-112.3-110.5 0-27.5 7.6-41.3 23.6-58.9-2.6-6.5-11.1-33.3 2.6-67.9 20.9-6.5 69 27 69 27 20-5.6 41.5-8.5 62.8-8.5s42.8 2.9 62.8 8.5c0 0 48.1-33.6 69-27 13.7 34.7 5.2 61.4 2.6 67.9 16 17.7 25.8 31.5 25.8 58.9 0 96.5-58.9 104.2-114.8 110.5 9.2 7.9 17 22.9 17 46.4 0 33.7-.3 75.4-.3 83.6 0 6.5 4.6 14.4 17.3 12.1C428.2 457.8 496 362.9 496 252 496 113.3 383.5 8 244.8 8zM97.2 352.9c-1.3 1-1 3.3.7 5.2 1.6 1.6 3.9 2.3 5.2 1 1.3-1 1-3.3-.7-5.2-1.6-1.6-3.9-2.3-5.2-1zm-10.8-8.1c-.7 1.3.3 2.9 2.3 3.9 1.6 1 3.6.7 4.3-.7.7-1.3-.3-2.9-2.3-3.9-2-.6-3.6-.3-4.3.7zm32.4 35.6c-1.6 1.3-1 4.3 1.3 6.2 2.3 2.3 5.2 2.6 6.5 1 1.3-1.3.7-4.3-1.3-6.2-2.2-2.3-5.2-2.6-6.5-1zm-11.4-14.7c-1.6 1-1.6 3.6 0 5.9 1.6 2.3 4.3 3.3 5.6 2.3 1.6-1.3 1.6-3.9 0-6.2-1.4-2.3-4-3.3-5.6-2z"
                        }
                    }
                }
                span{
                    class : "title",
                    "Fork On Github"
                }

            }

        }
    };

    component = match *home {
        "login" => login_user,
        "home" => home_user,
        "register" => register_user,
        "implementations" => new_implementation,
        "exit_implementation" => exit_implementation,
        "explore_realworld" => exit_implementation,
        _ => {
            rsx! {
        div {
            class : "mele",
            onclick : move |evt| _set_home("login"),
            "{home}"
        }
    }
        }
    };
    return cx.render(component);
}