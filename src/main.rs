extern crate dioxus;

mod components;

fn main() {
    dioxus::web::launch(components::app);
}


